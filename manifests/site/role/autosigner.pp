class role::autosigner(
  Sensitive $jwt_token,
  ){
  $confdir = '/etc/puppetlabs/puppet'
  $legacy_autosign_script_path = '/etc/legacy_autosign/autosign.py'
  class {'::autosign':
    manage_package => true,
    gem_source     => lookup('gem_source', {default_value => 'https://rubygems.org'}),
    config         => Sensitive.new({
      general     => {
        loglevel  => 'DEBUG',
        validation_order => ['jwt_token']
      },
      multiplexer => {
        external_policy_executable => $legacy_autosign_script_path
      },
      # default values for the token
      jwt_token   => {
        secret   => $jwt_token.unwrap,
        validity => '7200',
      }
    })
  }
  ini_setting { 'policy-based autosigning':
    setting => 'autosign',
    path    => "${confdir}/puppet.conf",
    section => 'master',
    value   => '/opt/puppetlabs/puppet/bin/autosign-validator',
    notify  => Service['pe-puppetserver'],
    require => Class['autosign']
  }
}
