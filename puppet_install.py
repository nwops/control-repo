#!/usr/bin/env python

import requests
import json
import os
import yaml
import time

PE_HOST="pe-xl-core-0.puppet.vm"
RBAC_PORT=4433
CLASSIFIER_PORT=4433
ORCH_PORT=8143
INVENTORY_PORT=8143

def get_event_data(auth_token, job_id):
  headers = {
      'Content-Type': 'application/json',
      'X-Authentication': auth_token,
  }
  response = requests.get(f'https://{PE_HOST}:{ORCH_PORT}/orchestrator/v1/jobs/{job_id}/events', headers=headers, verify=False)
  if response.ok:
    return response.json()


def run_task(auth_token, params, scope, task_name = "autosign::generate_token", environment = 'production'):
  headers = {
      'Content-Type': 'application/json',
      'X-Authentication': auth_token,
  }
  data = json.dumps({"task": task_name,"params": params,
   "environment": environment, "scope": scope})
  response = requests.post(f'https://{PE_HOST}:{ORCH_PORT}/orchestrator/v1/command/task', headers=headers, data=data, verify=False)
  if response.ok:
    return response.json()

def get_rbac_token(user = 'admin', password = 'puppetlabs'):
  headers = {
    'Content-Type': 'application/json',
  }
  data = json.dumps({"login": user,"password": password ,"lifetime":"24h"})
  response = requests.post(f'https://{PE_HOST}:{RBAC_PORT}/rbac-api/v1/auth/token', 
    headers=headers, data=data, verify=False
  )
  if response.ok:
    return response.json()['token']
  
def generate_signing_token(auth_token, certname="417c0c70c2d6.lan"):
  data = run_task(auth_token, {"certname":certname, "validfor": 7200},{"nodes":[PE_HOST]}, "autosign::generate_token")
  # {'job': {'id': 'https://pe-xl-core-0.puppet.vm:8143/orchestrator/v1/jobs/26', 'name': '26'}}
  return data['job']['name']
    

def node_group(auth_token, name):
  headers = {
      'Content-Type': 'application/json',
      'X-Authentication': auth_token,
  }
  response = requests.get(f'https://{PE_HOST}:{RBAC_PORT}/classifier-api/v1/groups', headers=headers, verify=False)
  if response.ok:
    for g in response.json():
      if g['name'].lower() == name.lower():
        return g

def job_data(auth_token, job_id):
  headers = {
      'Content-Type': 'application/json',
      'X-Authentication': auth_token,
  }
  response = requests.get(f'https://{PE_HOST}:{INVENTORY_PORT}/orchestrator/v1/jobs/{job_id}/events', headers=headers, verify=False)
  if response.ok:
    return response.json()

def create_csr_attribute(token):

  return {"custom_attributes": {
    "challengePassword": token
  } }

def create_role(auth_token, role_type = 'tasks', name, desc, task_name, user_ids=[], group_ids=[]):
  headers = {
      'Content-Type': 'application/json',
      'X-Authentication': '$TOKEN',
  }
  data = json.dumps({"permissions":[{"object_type": role_type, "action": task_name,"instance":"1"}],
  "group_ids":group_ids,"display_name": name,"user_ids":user_ids,
  "description": desc})
  response = requests.post(f'https://{PE_HOST}:$RBAC_PORT/rbac-api/v1/roles', headers=headers, data=data, verify=False)
    
    

def create_csr_file(csr_attributes, existing = dict()):
  if not os.path.exists('/etc/puppetlabs'):
    os.mkdir('/etc/puppetlabs') 
  if not os.path.exists('/etc/puppetlabs/puppet'):
    os.mkdir('/etc/puppetlabs/puppet')
  if os.path.exists('/etc/puppetlabs/puppet/csr_attributes.yaml'):
    with open('/etc/puppetlabs/puppet/csr_attributes.yaml', 'r') as a_reader:
      existing = yaml.load(a_reader.read(), Loader=yaml.SafeLoader) or dict()

  attributes = {**existing, **csr_attributes}
  print(yaml.dump(attributes))  
  # with open('/etc/puppetlabs/puppet/csr_attributes.yaml', 'w') as a_writer:
  #   a_writer.write(yaml.dump(attributes))

    
  

def get_signing_token():
  auth_token = get_rbac_token()

  #group = node_group(auth_token, 'PE master', )
  job_id = generate_signing_token(auth_token, os.uname()[1])
  # wait a few seconds as this is an async call
  time.sleep(10)
  event_data = get_event_data(auth_token, job_id)
  data = event_data['items'][1]['details']['detail']['_output'].strip()
  # the csr_data will need to be merged with the existing data found in the csr_attributes.yaml file
  csr_data = create_csr_attribute(data)
  create_csr_file(csr_data)

# def create_pe_console_permissions():
#   group = node_group(auth_token, 'PE master', )
#   group['uuid']
#   create_role(auth_token, 'tasks', 'Token Generator', "Generate tokens for provisioning",
#    "autosign::generate_token", user_ids=[], group_ids=[])



# # create permission
# 1. get PE Certificate Authority group id
# 2. create user unless already created
# 3. get user id
# 3. create a new role
# 4. add user to role
# 5. add task permission to role
#   1. run task
#   2. task name
#   3. node group